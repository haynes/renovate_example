module.exports = {
    platform: 'gitlab',
    endpoint: 'https://git.example.com/api/v4/',
    "packageRules": [{
        "managers": ["maven"],
        "registryUrls": ["https://artifactory1.example.com/artifactory/repo1/", "https://artifactory1.example.com/artifactory/repo2/"]
    }],
    hostRules:[{
        "platform": "maven",
        "endpoint": "https://artifactory1.example.com",
        "username": "artifactoryusername",
        "password": "artifactorypassword"
    }],
    onboardingConfig: { extends: ["config:base"]}
};
