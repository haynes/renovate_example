# renovate_example

Secret Variables:

ARTIFACTORY_USER

ARTIFACTORY_PWD

GITHUB_COM_TOKEN (A github access token so renovate can load CHANGELOGS from github open source projects.)

RENOVATE_TOKEN (An access token of the user you want renovate to create the MRs with. That user needs access to the projects listed in repositories.txt obviously.)


After configuring everything we've setup a scheduled job that runs once per day for this repo.
